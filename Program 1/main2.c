#include <stdio.h>
#include <stdlib.h>
#include "funkcje.h"
#include <math.h>
struct Wyniki
{
    float srednia_wyniki;
    float mediana_wyniki;
    float odchylenie_wyniki;

};

void wczytaj(FILE *plik, float *px, float *py, float *prho);

int main()
{
    float *x;
    float *y;
    float *rho;

   x=(float*)malloc(50 * sizeof(float));
   y=(float*)malloc(50 * sizeof(float));
   rho=(float*)malloc(50 * sizeof(float));


    FILE *plik;
    if((plik = fopen("P0001_attr.rec", "a+"))==NULL)
        printf("Blad podczas otwarcia pliku!");
    else
    {
        wczytaj(plik,x,y,rho);

        struct Wyniki a;
        a.srednia_wyniki = srednia(x);
        a.mediana_wyniki = mediana(x);
        a.odchylenie_wyniki = odchylenie(x);

        struct Wyniki b;
        b.srednia_wyniki = srednia(y);
        b.mediana_wyniki = mediana(y);
        b.odchylenie_wyniki = odchylenie(y);

        struct Wyniki c;
        c.srednia_wyniki = srednia(rho);
        c.mediana_wyniki = mediana(rho);
        c.odchylenie_wyniki = odchylenie(rho);

    }


    free(x);
    free(y);
    free(rho);
    fclose(plik);

    return 0;
}
void wczytaj(FILE *plik, float *px, float *py, float *prho)
{
    char a[4] = "";
    int i = 0;

    while(fscanf(plik, "%s %f %f %f", a, &px[i], &py[i], &prho[i])>0)
        {
            printf("%s %f %f %f \n", a, px[i], py[i], prho[i]);
            i++;
        }

    return;
}
